<?php
    // Diret�rio onde ficar�o os arquivos do sistema
    define('CLASS_DIR', 'App/');
    
    // Adicionando os arquivos PHP aos include paths
    set_include_path(get_include_path().PATH_SEPARATOR.CLASS_DIR);
    
    // You can use this trick to make autoloader look for commonly used "My.class.php" type filenames
    //spl_autoload_extensions('.php');
    
    // Utilizando implementa��o padr�o do autoload.
    spl_autoload_register();
?>