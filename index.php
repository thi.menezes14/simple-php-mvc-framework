<?php

    date_default_timezone_set('America/Sao_Paulo');
    session_start();
    
    //Faz o autoload de todas as classes no PHP
    require_once ('autoload.php');
    use App\route\Router;
    
    //Faz o dispatch da rota encontrada
    Router::dispatch();
?>