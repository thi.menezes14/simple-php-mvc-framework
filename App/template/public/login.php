<!DOCTYPE html>
<html>
<head>
	<?php require_once __DIR__ . '\\..\\base\\head.php' ?>
	<link rel="stylesheet" href="public/assets/css/login.css" />
</head>
<body>
	<div class="login container-fluid">
		<div class="row">
			<div class="col-md-12 col-lg-6 d-flex flex-column align-items-center justify-content-center">

				<div class="form-group form-login text-light">
					<form method="POST" action="login">
						<div class="form-group">
							<label for="username">E-mail</label> 
							<input type="text" class="form-control" id="username" name="username" placeholder="Digite o seu e-mail" />
						</div>
						<div class="form-group">
							<label for="password">Senha</label> 
							<input type="password" class="form-control" id="password" name="password" placeholder="Digite a sua senha" />
						</div>
						<button id="btn-login" type="submit" class="btn btn-primary">Entrar</button>
					</form>
					<br />
					<p><a href="home" class="text-light">Voltar para Home</a></p>
				</div>
				
				<?php if(isset($erro)) { ?>
    				<div id="erro" class="alert alert-danger" role="alert">
    					<p>Aten��o: <strong><?=$erro?></strong></p>
    				</div>
    			<?php } ?>
    		
			</div>
			<div class="col-md-12 col-lg-6 login-image"></div>
		</div>
	</div>
</body>
</html>