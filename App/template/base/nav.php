<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Meu Framework PHP</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" aria-current="home" href="home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="sobre" href="sobre">Sobre</a>
        </li>
        <li class="nav-item ml-auto">
          <a class="nav-link" aria-current="sobre" href="login">Login</a>
        </li>
      </ul>
    </div>
  </div>
</nav>