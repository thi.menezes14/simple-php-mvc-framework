<?php if(isset($username)) { ?>
	<ul class="nav nav-logged">
      <li class="nav-item">
        <a class="nav-link" href="#">Meu Espa�o</a>
      </li>
      <li class="nav-item dropdown ms-auto">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Meu Usu�rio
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          	<ul class="nav">
          		<li class="nav-item"><p>Usu�rio: <strong><?=$username?></strong></p></li>
          		<li class="nav-item"><p>Login �s <?=$login_date?></p></li>
          		<li class="nav-item"><a role="button" class="btn btn-info" href="logout">Sair</a></li>
          	</ul>	
        </div>
      </li>
	</ul>
<?php } ?>