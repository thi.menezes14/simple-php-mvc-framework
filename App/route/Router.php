<?php
namespace App\route;

use App\controller\NotFoundController;

/*
 * C�digo que inspirou a verifica��o de par�metros din�micos:
 * https://github.com/zachlankton/zach_writes_code/blob/d003c9c31cf14887d405895c506cf1c1b0b2e12f/classes/router.php#L76
 * */

abstract class Router {
    
    /*Array que guardar� as rotas da aplica��o.
     * O seu �ndice ser� composto da seguinte informa��o:
     * 'uri' . '@' . 'metodo'.
     * Deste modo, os arrays associativos que possu�rem a mesma URI,
     * mas diferentes m�todos, n�o se sobrescrever�o.
     * */
    private static $routes = array();
    
    /*
     * Registra uma rota na aplica��o. 
     * O �ndice � composto pela uri e o m�todo da requisi��o,
     * separados por um '@'. Isto tem a finalidade de permitir uma 
     * mesma rota com mais de um m�todo HTTP.
     * */
    public static function register($uri, $method, $callback) {
        self::$routes[$uri.'@'.$method] = $callback;
    }
    
    /*
     * Trata a URL acessada, acessa o �ndice, separa as informa��es
     * de URI e m�todo que estavam no �ndice e verifica se URI e m�todo acessados
     * correspondem � URL acessada. Se for o caso, s�o tratados os arrays de GET e POST
     * (para evitar ataques XSS) e invocada a fun��o de callback registrada.
     * */
    public static function dispatch() {
        
        //Registra todos os componentes de rota
        RouteRegister::register();
        
        //Rota obtida e tratada via navegador.
        $req_uri = self::parse_uri();
        
        //Vari�vel que controla se URL solicitada foi encontrada
        $route_found = true;
        
        //Array associativo para cada parte din�mica da rota que cont�m o seu valor.
        $path_params = array();
        
        //Para cada item do array associativo "uri@metodo" e que carrega consigo o seu callback, fa�a:
        foreach(self::$routes as $index => $callback) {
            
            //Separando URI e m�todo de acordo com o �ndice do explode
            $route_data = explode('@', $index);
            $uri_compared = $route_data[0];
            $uri_method = $route_data[1];
            
            //Cria um array com todos os par�metros (palavras entre as barras) da rota.
            $uri_compared_path_params = explode('/', $uri_compared);
            //Retirando primeiro elemento do array que sempre conter� uma string vazia
            array_shift($uri_compared_path_params);
            
            foreach($uri_compared_path_params as $key => $uri_section) {
                        
                if(!self::is_route_same_length($uri_compared_path_params, $req_uri)) {
                    $route_found = false;
                    continue;
                }
           
                //Verifica as partes din�micas da rota e cria um array associativo com "nome_da_parte_dinamica => trecho_da_rota_solicitada"
                if ( strpos($uri_compared_path_params[$key], ":") !== false ) {
                    $path_params[substr($uri_compared_path_params[$key], 1)] = $req_uri[$key];
                    continue;
                }
                
                $route_found = $uri_section === $req_uri[$key];
            }
            
            //Se rota foi encontrada e m�todo da requisi��o tamb�m corresponder, execute o callback.
            if($route_found && strtoupper($uri_method) === $_SERVER['REQUEST_METHOD']) {
                $get_params = self::escape_special_chars_from_params($_GET);
                $post_params = self::escape_special_chars_from_params($_POST);
                echo $callback($get_params, $post_params, $path_params);
                break;
            }
            
        }
        
        if(!$route_found) {
            $not_found_controller = new NotFoundController();
            echo $not_found_controller->index();
        }
    }
    
    /*
     * Redireciona para outra rota (�til para rotas que mostrar�o o mesmo template)
     * */
    public static function redirect($new_location) {
        header('Location: ' . $new_location);
        exit();
    }
    
    /*
     * Trata a URL. � recuperado apenas o trecho necess�rio para acessar os recursos registrados nas rotas.
     * Como nosso .htaccess registra a URI como um par�metro de query string, ele � removido do array $_GET.
     * Finalmente, para ignorar a �ltima barra que pode ser passada, a removemos da string resultante da URI tratada.
     * */
    private function parse_uri() {
        $splitted_uri = explode('/', $_SERVER['REQUEST_URI']);
        $uri_params_with_query_string = array_slice($splitted_uri, 3);
        $uri_params = explode('?', '/' .implode("/", $uri_params_with_query_string))[0];
        unset($_GET[$uri_params]);
        $rtrimmed_uri_params = rtrim($uri_params , '/');
        $filtered_uri_params = explode('/', $rtrimmed_uri_params);
        array_shift($filtered_uri_params);
        return $filtered_uri_params;
    }
    
    /*
     * Pega valores de um array $_GET ou $_POST e trata tags e outros caracteres especiais de 
     * HTML, CSS ou JS como strings (ver fun��o 'htmlspecialchars'). Isto evita inje��es de c�digo e ataques do tipo XSS.
     * Veja em: https://medium.com/@rafaelrenovaci/cross-site-scripting-xss-4e3851b906ae
     * */
    private function escape_special_chars_from_params($arr) {
        $sanitized_array = array();
        foreach ($arr as $key => $value) {
            $sanitized_array[$key] = htmlspecialchars($value);
        }
        return $sanitized_array;
    }
    
    /*
     * Fun��o que verifica, ap�s separar as barras, se URI solicitada e URI comparada
     * t�m o mesmo n�mero de argumentos.
     * 
     * */
    private function is_route_same_length($uri_requested, $uri_compared) {
        return count($uri_requested) === count($uri_compared);
    }
}

