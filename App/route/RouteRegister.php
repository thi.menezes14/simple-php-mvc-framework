<?php
namespace App\route;

use App\route\groups\PublicRouter;
use App\route\groups\UsuarioRouter;

/*
 * Esta classe reune todos os Routers, funcionando como
 * um "wrapper", isto �, uma classe que possui o objetivo de 
 * "embrulhar", ou seja, reunir um conjunto de outras classes e
 * realizar uma mesma a��o em conjunto.
 * */
class RouteRegister implements iRouterUtils {
    public static function register() {
        PublicRouter::register();
        UsuarioRouter::register();
    }
}

