<?php
namespace App\route\groups;

use App\controller\HomeController;
use App\controller\SobreController;
use App\route\Router;
use App\route\iRouterUtils;
use App\controller\LoginController;

class PublicRouter implements iRouterUtils {
    
    private static HomeController $home_controller;
    private static SobreController $sobre_controller;
    private static LoginController $login_controller;
    
    public static function register() {
        self::$home_controller = new HomeController();
        self::$sobre_controller = new SobreController();
        self::$login_controller = new LoginController();
        
        Router::register('/home', 'GET', function ($get_params, $post_params) {
            return self::$home_controller->index($get_params, $post_params);
        });
        Router::register('/sobre', 'GET', function ($get_params, $post_params) {
            return self::$sobre_controller->index($get_params, $post_params);
        });
        Router::register('/login', 'GET', function ($get_params, $post_params) {
            return self::$login_controller->index($get_params, $post_params);
        });
        Router::register('/login', 'POST', function ($get_params, $post_params) {
            return self::$login_controller->authenticate($post_params);
        });
        Router::register('/logout', 'GET', function ($get_params, $post_params) {
            return self::$login_controller->unauthenticate();
        });
    }
}

