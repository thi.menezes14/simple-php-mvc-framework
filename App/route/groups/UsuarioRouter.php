<?php
namespace App\route\groups;

use App\controller\UsuarioController;
use App\route\Router;
use App\route\iRouterUtils;

class UsuarioRouter implements iRouterUtils {

    private static UsuarioController $controller;

    public static function register() {
        self::$controller = new UsuarioController();

        Router::register('/usuarios', 'GET', function ($get_params, $post_params) {
            return self::$controller->index($get_params, $post_params, 'Wowowowow');
        });
        Router::register('/usuarios/novo', 'GET', function ($get_params, $post_params) {
            return self::$controller->novo($get_params, $post_params, 'Yupiiiii');
        });
        Router::register('/usuarios/:codigo', 'GET', function($get_params, $post_params, $path_variables) {
            echo 'YEY-YEY-YEY';
        });
        Router::register('/usuarios/:codigo/editar', 'GET', function($get_params, $post_params, $path_variables) {
            echo 'WOW-WOW-WOW';
        });
        
    }
}

