<?php
namespace App\route;

interface iRouterUtils {
    /*
     * Esta interface implemeta um m�todo obrigat�rio para
     * as classes que fizerem o papel de Router.
     * Este m�todo obriga os Routers a registrarem as rotas
     * para acessar os recursos de um contexto espec�fico.
     * */
    public static function register();
}

