<?php
namespace App\view;

//Classe-base onde todas as views ter�o acesso �s funcionalidades de manipula��o e constru��o das views.
class AppView {
    
    private $data = array(); //Vari�veis que ser�o utilizadas no template.
    private $context; //O contexto do template. No caso, o diret�rio.
    private $template; //Nome do arquivo .php que consistir� no template.

    public function __construct($context) {
        $this->context = $context;
    }
    
    /*
     * Adicionamos as vari�veis que ser�o utilizadas pelo template.
     * Cada �ndice do array associativo ser� o nome de uma vari�vel.
     * */
    public function addObject($key, $value) {
       $this->data[$key] = $value;
    }
    
    /*
     * Constru�mos aqui a view por meio de um buffer onde inclu�mos o arquivo e as vari�veis 
     * que passamos no atributo $data.
     * */
    public function buildView() {
        ob_start();
        extract($this->data);
        $str_require = __DIR__ . '\\..\\template\\'. $this->context . '\\' . $this->template .'.php';
        require($str_require);
        return utf8_encode(ob_get_clean());
    }
    
    /*
     * Definimos qual template (p�gina) ser� utilizada na constru��o da nossa view.
     * */
    public function setTemplate($template) {
        $this->template = $template;
    }
}

