<?php
namespace App\controller;

class SobreController extends DefaultController {
    
    public function __construct() {
        parent::__construct("public");
    }
    
    public function index($get_params = null, $post_params = null) {
        
        $this->view->setTemplate("sobre");
        $this->view->addObject('title', 'Sobre o Sistema');
        $this->view->addObject('nome', 'Thiago');
        return $this->view->buildView();
    }
}

