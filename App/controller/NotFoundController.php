<?php
namespace App\controller;

use App\view\AppView;

class NotFoundController {

    private AppView $view;
    
    public function __construct() {
        $this->view = new AppView("error");
    }
    
    public function index() {
        $this->view->setTemplate("404");
        return $this->view->buildView();
    }
}

