<?php
namespace App\controller;

use App\route\Router;

class LoginController extends DefaultController {
    
    public function __construct() {
        parent::__construct("public");
    }
    
    public function index($get_params = null, $post_params = null) {
        $this->view->setTemplate("login");
        $this->view->addObject('title', 'Login');
        return $this->view->buildView();
    }
    
    public function authenticate($post_params) {
        
        $username = $post_params['username'];
        $password = $post_params['password'];
        
        //TODO: substituir esta parte pela autentica��o real
        if($username === "admin" && $password === "12345") {
            $this->set_session_data('username', $username);
            $this->set_session_data('login_date', date('d/m/Y H:i:s'));
            Router::redirect("home?login=true");
        } else {
            $this->view->setTemplate("login");
            $this->view->addObject('title', 'Login');
            $this->view->addObject('erro', 'Usu�rio e/ou senha incorretos. ');
            return $this->view->buildView();
        }
        
    }
    
    public function unauthenticate() {
        $this->destroy_session();
        Router::redirect("login");
    }
}

