<?php
namespace App\controller;

use App\view\AppView;

class UsuarioController {
    
    private AppView $view;

    public function __construct() {
        $this->view = new AppView("usuarios");
    }
    
    public function index($get_params = null, $post_params = null, $palavra) {
        $this->view->setTemplate("index");
        $this->view->addObject("teste", 'Ol�');
        $this->view->addObject("palavra", $palavra);
        return $this->view->buildView();
    }
    
    public function novo($query_params = null) {
        $this->view->setTemplate("novo");
        $this->view->addObject("teste", "Show de bola");
        return $this->view->buildView();
    }
}

