<?php
namespace App\controller;

class HomeController extends DefaultController {
    
    public function __construct() {
        parent::__construct("public");
    }
    
    public function index($get_params = null, $post_params = null) {
        $this->view->setTemplate("home");
        $this->view->addObject('title', 'P�gina Inicial');
        return $this->view->buildView();
    }
}

