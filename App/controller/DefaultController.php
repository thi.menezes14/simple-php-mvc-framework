<?php
namespace App\controller;

use App\view\AppView;

abstract class DefaultController implements iBaseController {
    
    protected AppView $view;
    
    public function __construct($pathname) {
        $this->view = new AppView($pathname);
        $this->get_session_data();
    }
    
    public function index() {
        return null;
    }
    
    private function get_session_data() {
        foreach($_SESSION as $key => $value) {
            $this->view->addObject($key, $value);
        }
    }
    
    protected function set_session_data($session_key, $session_value) {
        $_SESSION[$session_key] = $session_value;
    }
    
    protected function destroy_session() {
        session_destroy();
    }
}

